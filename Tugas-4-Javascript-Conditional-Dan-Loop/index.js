// Soal 1
var nilai = 90;
if (nilai >= 85 ) {
    console.log("A")
} else if (nilai >= 75 && nilai < 85) {
    console.log("B")
} else if (nilai >= 65 && nilai < 75) {
    console.log("C")
} else if (nilai >= 55 && nilai < 65) {
    console.log("d")
} else if (nilai < 55 ) {
    console.log("E")
} else {
    console.log("Format Index Salah");
}
console.log("==============================")

// Soal 2
var tanggal = 12;
var bulan = 02;
var tahun = 1998;
switch(bulan) {
  case 01:   { console.log('12 Januari 1998'); break; }
  case 02:   { console.log('12 Februari 1998'); break; }
  case 03:   { console.log('12 Maret 1998'); break; }
  case 04:   { console.log('12 April 1998'); break; }
  case 05:   { console.log('12 Mei 1998'); break; }
  case 06:   { console.log('12 Juni 1998'); break; }
  case 07:   { console.log('12 Juli 1998'); break; }
  case 08:   { console.log('12 Agustus 1998'); break; }
  case 09:   { console.log('12 September 1998'); break; }
  case 10:   { console.log('12 Oktober 1998'); break; }
  case 11:   { console.log('12 November 1998'); break; }
  case 12:   { console.log('12 Desember 1998'); break; }
  default:  { console.log('Tanggal Lahir Tidak Ada'); }
}
console.log("==============================")

// Soal 3
var show = '';
for (var i = 1; i <= 3; i++) {
    for (var j = 1; j <= i; j++){
        show += '#';
    }
    show += '\n';
}
console.log(show);

var show = '';
for (var i = 1; i <= 7; i++) {
    for (var j = 1; j <= i; j++){
        show += '#';
    }
    show += '\n';
}
console.log(show);
console.log("==============================")

// Soal 4
var kata = ["Programming", "Javascript", "VueJS"];

var panjang = kata.length;
var index = 0;
var nomor = 1;
masukan = 10;
while (nomor <= masukan) {
  console.log(nomor + " - I love " + kata[index]);
  index += 1;
  nomor += 1;

  if (index == panjang) {
    console.log("=".repeat(nomor - 1));
    index = 0;
  }
}