// Soal 1
const luas = (a, b) => {
  let panjang = a;
  let lebar = b;

  return panjang * lebar;
};

const keliling = (a, b) => {
  let panjang = a;
  let lebar = b;

  return 2 * (panjang + lebar);
};

console.log(luas(6, 8));
console.log(keliling(3, 7));

console.log("==============================")

// Soal 2
const newFunction = (firstName, lastName) => {
  return {
    fullName: () => {
      console.log(`${firstName} ${lastName}`)
    },
  };
};
   
console.log(newFunction("William", "Imoh").fullName());

console.log("==============================")

// Soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject;
console.log(firstName, lastName, address, hobby);

console.log("==============================")

// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

console.log(combined);

console.log("==============================")

// Soal 5
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

console.log(before);