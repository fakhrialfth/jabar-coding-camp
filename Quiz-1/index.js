// Soal 1
var kalimat_1 = "Halo nama saya Fakhri Al Fatah"
var kalimat_2 = "Saya Fakhri"
var kalimat_3 = "Saya Fakhri Al Fatah"

function jumlahKata(n) {
    return n.trim().split(" ").length;
}

console.log(jumlahKata(kalimat_1));
console.log(jumlahKata(kalimat_2));
console.log(jumlahKata(kalimat_3));

console.log("==============================")

// Soal 2
function nextDate(tanggal, bulan, tahun) {
    var day = tanggal + 1;
    var mounth = bulan;
    var year = tahun

    switch(mounth) {
        case 1:
            mounth = "Januari";
            break;
        case 2:
            mounth = "Fbruari";
            if (day === 30) {
                day = 1;
                mounth = "Maret";
            } if (day === 29 && year === 2021) {
                day = 1;
                mounth = "Maret";
            }
            break;
        case 3:
            mounth = "Maret"
            break;
        case 4:
            mounth = "April"
            break;
        case 5:
            mounth = "Mei"
            break;
        case 6:
            mounth = "Juni"
            break;
        case 7:
            mounth = "Juli"
            break;
        case 8:
            mounth = "Agustus"
            break;
        case 9:
            mounth = "September"
            break;
        case 10:
            mounth = "Oktober"
            break;
        case 11:
            mounth = "November"
            break;
        case 12:
            mounth = "Desember"
            if (day === 32) {
                day = 1;
                mounth = "Januari"
                year += 1;
            }
            break;
            default:
            break;
    }
    return String(day) + " " + mounth + " " + String(year);
}

var tanggal = 29;
var bulan = 2;
var tahun = 2020;

console.log(nextDate(tanggal, bulan, tahun));

var tanggal = 28;
var bulan = 2;
var tahun = 2021;

console.log(nextDate(tanggal, bulan, tahun));

var tanggal = 31;
var bulan = 12;
var tahun = 2020;

console.log(nextDate(tanggal, bulan, tahun));